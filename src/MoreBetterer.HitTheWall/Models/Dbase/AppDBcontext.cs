﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MoreBetterer.HitTheWall.Models
{
    public class AppDBcontext: IdentityDbContext<IdentityUser>
    {

        public AppDBcontext(DbContextOptions<AppDBcontext> options) : base(options)
        { }



      public DbSet<GameMap> GameMaps { get; set;}
        public DbSet<GameTile> GameTiles { get; set; }
        public DbSet<GamePlayer> GamePlayers { get; set; }
    }
}
