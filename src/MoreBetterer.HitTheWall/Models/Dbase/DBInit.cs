﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoreBetterer.HitTheWall.Models;

namespace MoreBetterer.HitTheWall.Models.Dbase
{
    public class DBInit
    {

        public static void Seed(AppDBcontext context)
        {


            if (!context.GameMaps.Any())
            {

                context.AddRange(
                 new GameMap { MapName = "Demo", CreatedBy = "Peter M.", File = "?", Retired = false,
                     MaxHits= 10,
                     MaxObjects= 4,
                 }
                     );

                context.SaveChanges();


            }

            var gamemapid = context.GameMaps.Last().GameMapID;

            if (!context.GameTiles.Any())
            {
                
                context.AddRange(
                 new GameTile { GameMapID = gamemapid, RoomID = 1, RoomName = "Entrance", RoomObject = "Mirror", xcoord =3, ycoord=2,N=2,E=0,S=0,W=0 },
                 new GameTile { GameMapID = gamemapid, RoomID = 2, RoomName = "Dining Room", RoomObject = "Table", xcoord = 2, ycoord = 2, N = 0, E = 3, S = 1, W = 4 },
                 new GameTile { GameMapID = gamemapid, RoomID = 3, RoomName = "Kitchen", RoomObject = "Knife", xcoord = 2, ycoord = 3, N = 0, E = 0, S = 0, W = 2 },
                 new GameTile { GameMapID = gamemapid, RoomID = 4, RoomName = "Hallway", RoomObject = "Potted Plant", xcoord = 2, ycoord = 1, N = 5, E = 2, S = 0, W = 0 },
                 new GameTile { GameMapID = gamemapid, RoomID = 5, RoomName = "Living Room", RoomObject = "Book", xcoord = 1, ycoord = 1, N = 0, E = 0, S = 4, W = 6 },
                 new GameTile { GameMapID = gamemapid, RoomID = 6, RoomName = "Master Bedroom", RoomObject = "Lamp", xcoord = 1, ycoord = 0, N = 7, E = 5, S = 0, W = 0 },
                 new GameTile { GameMapID = gamemapid, RoomID = 7, RoomName = "Patio", RoomObject = "Chaise-Lounge", xcoord = 0, ycoord = 0, N = 0, E = 0, S = 6, W = 0 }

                     );

                context.SaveChanges();


            }

        }
    }
}
