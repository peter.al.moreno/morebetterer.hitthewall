﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoreBetterer.HitTheWall.Models
{
    public class GamePlayer
    {
        public int GamePlayerID { get; set; }

        //ID and customizations
        [MaxLength(20)]
        public string PlayerName { get; set; }
        [MaxLength(20)]
        public string CreatedBy { get; set; }



        //relative to Status druing play
        public int CurrentMapMaxHits { get; set; } // updated upon start of new game. Less DB calls if its just included here.
        public int CurrentHits { get; set; }
        public int CurrentMapMaxObjects { get; set; }
        public int CurrentObjects { get; set; }
        public int CurrentRoom { get; set; }
        public int CurrentMap{ get; set; }  // reference map settings to compare MAX hits and MAX objects. 
        public bool IsPlaying { get; set; }
        public string PlayerStatus { get; set; } //Shows if player is Winner, Loser, Playing

        //Tracking Game
        public string ObjectsCollected { get; set; } //will convert string-> list in application
        public string ObjectsNeeded { get; set; }
        public string PathTracker{ get; set; }


        //relative to Status overall
        public int GamesWon { get; set; }
        public int GamesLost { get; set; }
        


    }
}
