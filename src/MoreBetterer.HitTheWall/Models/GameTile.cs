﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoreBetterer.HitTheWall.Models
{
    public class GameTile
    {
        public int GameTileID { get; set; }

        public int GameMapID { get; set; }
        public virtual GameMap Game { get; set; }


        public int RoomID { get; set; }

        [MaxLength(20)]
        public string RoomName { get; set; }
        [MaxLength(20)]
        public string RoomObject { get; set; }

        public int xcoord { get; set; }
        public int ycoord { get; set; }
        public int N { get; set; }
        public int E { get; set; }
        public int S { get; set; }
        public int W { get; set; }

    }
}
