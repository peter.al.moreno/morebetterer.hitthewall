﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoreBetterer.HitTheWall.Models;

namespace MoreBetterer.HitTheWall.Models.Repos
{
    public interface IGamePlayerRepo
    {
        //base
        IEnumerable<GamePlayer> GetGamePlayers();
        GamePlayer GetByID(int gpID);
        GamePlayer GetLatest(); //temporary for testing
        void CreateGamePlayer(GamePlayer gp);
        void UpdateGamePlayer(GamePlayer gp);
        void RemoveGamePlayer(GamePlayer gp);

        //Status
        int HpCheck (int gpID);

        //move - actions that update database position etc
        string MovePlayer(int gpID, GameTile Room,string path); // Change Current room, update Trackers  
        string CheckVictory(int gpID);
        void oof(int gpID); // hit wall
        void GameQuit(int gpID);
        void GameStart(int gpID, GameTile Room);
        void GameLost(int gpID);
        void GameWon(int gpID);
        void SelectMap(int gpID,GameMap Map);
        void SetObjectsNeeded(int gpID, string objects);


        //Trackers - lists that track items, path, etc for player. Returns Lists from strings
        List<string> ObjectsCollected(int gpID);
        List<string> ObjectsNeeded(int gpID);
        List<string> PathTracker(int gpID);

       


    }
}
