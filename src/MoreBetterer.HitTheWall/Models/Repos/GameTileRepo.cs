﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoreBetterer.HitTheWall.Models.Repos
{
    public class GameTileRepo : IGameTileRepo
    {
        private readonly AppDBcontext _appDBcontext;

        public GameTileRepo(AppDBcontext appdb)
        {
            _appDBcontext = appdb;

        }
        public void CreateGameTile(GameTile GT)
        {
            _appDBcontext.GameTiles.Add(GT);
            _appDBcontext.SaveChanges();
        }

        public GameTile GetByID(int GTid)
        {
            return _appDBcontext.GameTiles.FirstOrDefault(p => p.GameTileID == GTid);
        }

        public int GetByRoomAndMap(int RMid, int GMid)
        {

            if (RMid == 0) 
            return 0;

            var temp = GetIGameTilesByGMapID(GMid);

            return temp.FirstOrDefault(p => p.RoomID == RMid).GameTileID;
        }

        public IEnumerable<GameTile> GetIGameTiles()
        {
            return _appDBcontext.GameTiles;
        }

        public IEnumerable<GameTile> GetIGameTilesByGMapID(int GMapID)
        {
            return _appDBcontext.GameTiles.Where(p=>p.GameMapID==GMapID);
        }

        public void RemoveGameTile(GameTile GT)
        {
            _appDBcontext.GameTiles.Remove(GT);
            _appDBcontext.SaveChanges();
        }

        public void UpdateGameTile(GameTile GT)
        {
            _appDBcontext.GameTiles.Update(GT);
            _appDBcontext.SaveChanges();
        }
    }
}
