﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoreBetterer.HitTheWall.Models;

namespace MoreBetterer.HitTheWall.Models.Repos
{
    public interface IGameTileRepo
    {

        IEnumerable<GameTile> GetIGameTiles();
        IEnumerable<GameTile> GetIGameTilesByGMapID(int GMapID);
        GameTile GetByID(int GTid);
        int GetByRoomAndMap(int Roomid,int GMid);
        void CreateGameTile(GameTile GT);
        void UpdateGameTile(GameTile GT);
        void RemoveGameTile(GameTile GT);
        
    }
}
