﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoreBetterer.HitTheWall.Models;

namespace MoreBetterer.HitTheWall.Models.Repos
{
    public class GamePlayerRepo : IGamePlayerRepo
    {


        private readonly AppDBcontext _appDBcontext;

        public GamePlayerRepo(AppDBcontext appdb)
        {
            _appDBcontext = appdb;

        }


        public void CreateGamePlayer(GamePlayer gp)
        {
            _appDBcontext.GamePlayers.Add(gp);
            _appDBcontext.SaveChanges();
        }

        public void GameLost(int gpID)
        {
            var player = this.GetByID(gpID);
            player.GamesLost++;
            player.PlayerStatus = "Lost";
            player.IsPlaying = false;
            UpdateGamePlayer(player);
        }

        public void GameQuit(int gpID)
        {
            var player = this.GetByID(gpID);
            player.GamesLost++;
            player.PlayerStatus = "Quit";
            player.IsPlaying = false;
            UpdateGamePlayer(player);
        }

        public void GameStart(int gpID, GameTile Room)
        {
            var player = this.GetByID(gpID);
            player.CurrentRoom = Room.GameTileID;
            player.CurrentHits = 0;
            player.CurrentObjects = 0;
            player.ObjectsCollected = "";

            if (CanCollect(gpID, Room.RoomObject))
            {
                player.ObjectsCollected += Room.RoomObject + ";";
                player.CurrentObjects++;
            }

            player.PathTracker = "Start->"+Room.RoomName+" ; ";
            player.PlayerStatus = "Playing";
            player.IsPlaying = true;
            UpdateGamePlayer(player);
        }

        public void GameWon(int gpID)
        {
            var player = this.GetByID(gpID);
            player.GamesWon++;
            player.PlayerStatus = "Won";
            player.IsPlaying = false;
            UpdateGamePlayer(player);
        }

        public GamePlayer GetByID(int gpID)
        {
            return _appDBcontext.GamePlayers.FirstOrDefault(p => p.GamePlayerID == gpID);
        }

        public IEnumerable<GamePlayer> GetGamePlayers()
        {
            return _appDBcontext.GamePlayers;
        }

        public GamePlayer GetLatest()
        {
            return _appDBcontext.GamePlayers.Last();
        }

        public int HpCheck(int gpID)
        {
            return _appDBcontext.GamePlayers.FirstOrDefault(p => p.GamePlayerID == gpID).CurrentHits;
        }

        public string MovePlayer(int gpID, GameTile Room, string path)
        {
            var player = this.GetByID(gpID);
            player.CurrentRoom = Room.GameTileID;

            if (CanCollect(gpID, Room.RoomObject))
            {
                player.ObjectsCollected += Room.RoomObject + ";";
                player.CurrentObjects++;
            }

            player.PathTracker += path + "->" + Room.RoomName + " ; ";
            UpdateGamePlayer(player);

            return "Moved";
        }


        private bool CanCollect(int gpID, string item)
        {
            var Needed= this.ObjectsNeeded(gpID);
            var Has=this.ObjectsCollected(gpID);

            if (Needed.Contains(item)&&!Has.Contains(item))
            {
                return true;
            }
            

            return false;


        }

        public string CheckVictory(int gpID)
        {
            var player = this.GetByID(gpID);
            if (player.CurrentObjects >= player.CurrentMapMaxObjects)
                return "Won";
            if (player.CurrentHits >= player.CurrentMapMaxHits)
                return "Lost";
                        
           

            return "pending";
        }







        public List<string> ObjectsCollected(int gpID)
        {
            var player = this.GetByID(gpID);
            string tempstring = player.ObjectsCollected;
            List<string> results = new List<string>();
            results = tempstring.Split(';').ToList();
                       
            return results;
        }

        public List<string> ObjectsNeeded(int gpID)
        {
            var player = this.GetByID(gpID);
            string tempstring = player.ObjectsNeeded;
            List<string> results = new List<string>();
            results = tempstring.Split(';').ToList();

            return results;
        }

        public List<string> PathTracker(int gpID)
        {
            var player = this.GetByID(gpID);
            string tempstring = player.PathTracker;
            List<string> results = new List<string>();
            results = tempstring.Split(';').ToList();

            return results;
        }

        public void RemoveGamePlayer(GamePlayer gp)
        {
            _appDBcontext.GamePlayers.Remove(gp);
            _appDBcontext.SaveChanges();
        }

        public void SelectMap(int gpID, GameMap Map)
        {
            var player = this.GetByID(gpID);
            player.CurrentMap = Map.GameMapID;
            player.CurrentMapMaxHits = Map.MaxHits;
            player.CurrentMapMaxObjects = Map.MaxObjects;
            
            UpdateGamePlayer(player);
        }

        public void SetObjectsNeeded(int gpID, string objects)
        {
            var player = this.GetByID(gpID);
            player.ObjectsNeeded = objects;
            UpdateGamePlayer(player);
        }

        public void UpdateGamePlayer(GamePlayer gp)
        {
            _appDBcontext.GamePlayers.Update(gp);
            _appDBcontext.SaveChanges();
        }

        public void oof(int gpID)
        {
            var player = this.GetByID(gpID);
            player.CurrentHits++;
           
            UpdateGamePlayer(player);
        }
    }
}
