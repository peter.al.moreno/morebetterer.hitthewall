﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoreBetterer.HitTheWall.Models;

namespace MoreBetterer.HitTheWall.Models.Repos
{
    public interface IGameMapRepo
    {
        IEnumerable<GameMap> GetGameMaps();
        GameMap GetByID(int gmapID);
        GameMap GetLatest();
        void CreateGameMap(GameMap gmap);
        void UpdateGameMap(GameMap gmap);
        void RemoveGameMap(GameMap gmap);

    }
}
