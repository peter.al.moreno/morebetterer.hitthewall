﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoreBetterer.HitTheWall.Models.Repos
{
    public class GameMapRepo : IGameMapRepo
    {

        private readonly AppDBcontext _appDBcontext;

        public GameMapRepo(AppDBcontext appdb)
        {
            _appDBcontext = appdb;

        }
        
        public void CreateGameMap(GameMap gmap)
        {
            _appDBcontext.GameMaps.Add(gmap);
            _appDBcontext.SaveChanges();
        }

        public GameMap GetByID(int gmapID)
        {
            return _appDBcontext.GameMaps.FirstOrDefault(p => p.GameMapID == gmapID);
        }

        public IEnumerable<GameMap> GetGameMaps()
        {
            return _appDBcontext.GameMaps;
        }

        public GameMap GetLatest()
        {
            return _appDBcontext.GameMaps.Last();
        }

        public void RemoveGameMap(GameMap gmap)
        {
            _appDBcontext.GameMaps.Remove(gmap);
            _appDBcontext.SaveChanges();
        }

        public void UpdateGameMap(GameMap gmap)
        {
            _appDBcontext.GameMaps.Update(gmap);
            _appDBcontext.SaveChanges();
        }
    }
}
