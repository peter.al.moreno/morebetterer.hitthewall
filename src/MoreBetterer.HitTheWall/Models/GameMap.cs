﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoreBetterer.HitTheWall.Models
{
    public class GameMap
    {
        public int GameMapID { get; set; }

        [MaxLength(20)]
        public string MapName { get; set; }

        [MaxLength(20)]
        public string CreatedBy { get; set; }

        public int MaxHits { get; set; }
        public int MaxObjects { get; set; }

        public string File { get; set; }

        public bool Retired { get; set; }

        public ICollection<GameTile> MapTiles { get; set; }

    }
}
