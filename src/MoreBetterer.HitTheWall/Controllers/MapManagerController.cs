﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoreBetterer.HitTheWall.Mapping;
using MoreBetterer.HitTheWall.ViewModels;
using MoreBetterer.HitTheWall.Models.Repos;
using MoreBetterer.HitTheWall.Models;
using Newtonsoft.Json;

namespace MoreBetterer.HitTheWall.Controllers
{
    public class MapManagerController : Controller
    {

        private readonly IGameMapRepo _gameMapRepo;
        private readonly IGameTileRepo _gameTileRepo;

        public MapManagerController(IGameMapRepo gmapRepo, IGameTileRepo gtRepo)
        {
            _gameMapRepo = gmapRepo;
            _gameTileRepo = gtRepo;

        }



        public IActionResult Index()
        {



            var map = _gameMapRepo.GetLatest();
            var tiles = _gameTileRepo.GetIGameTilesByGMapID(map.GameMapID).ToList();
            List<MapTile> mapTiles = new List<MapTile>();
            int columns = tiles.Max(p => p.ycoord)+1;
            int rows = tiles.Max(p => p.xcoord)+1;
            MapGrid mapGrid = new MapGrid(rows, columns);
            foreach (GameTile gt in tiles)
            {
                mapTiles.Add(new MapTile(gt.xcoord, gt.ycoord, gt.GameTileID, gt.GameTileID.ToString()));
            }

    
            var themap = mapGrid.UpdateGrid(mapTiles);
            
            var tempViewModel = new MapGridViewModel()
            {
                grid = themap,
                GameTiles= tiles,
                MapName=map.MapName,
                CreatedBy=map.CreatedBy,
                Maxhits=map.MaxHits,
                MaxObjects=map.MaxObjects

            };

            return View(tempViewModel);
        }




        [Route("[controller]/MapInfo")]
        [HttpGet]
        public IActionResult MapInfo()
        {
            var map = _gameMapRepo.GetLatest();
      
        string json = @"{'GameMapID':" + map.GameMapID + @",'MapName':'" + map.MapName+ @"','CreatedBy':'" + map.CreatedBy + @"'}";
            var content = JsonConvert.DeserializeObject(json);
            return Json(content);

        }


        [Route("[controller]/MapFile/{id}")]
        [HttpGet]
        public IActionResult Map(int id)
        {
            var map = _gameMapRepo.GetByID(id);
            var tiles = _gameTileRepo.GetIGameTilesByGMapID(map.GameMapID).ToList();

            // var content = JsonConvert.SerializeObject(_gameMapRepo.GetLatest());

            string tilestring = "";
            int counter = 1;
            foreach (GameTile gt in tiles)
            {
                tilestring += @"{'RoomID': " + gt.RoomID + @",'RoomName': '" + gt.RoomName + @"','RoomObject': '" + gt.RoomObject +
                    @"','xcoord': " + gt.xcoord + @",'ycoord': " + gt.ycoord + @",'N': " + gt.N + @",'E': " + gt.E + @",'S': " + gt.S + @",'W': " + gt.W + @" }";


                if (counter != tiles.LongCount())
                    tilestring += ",";
                counter++;
            }
            string json =
             @"{'MapName': '" + map.MapName + @"','CreatedBy':'" + map.CreatedBy + @"','MaxHits':" + map.MaxHits + @",'MaxObjects':" + map.MaxObjects + @",'MapTiles': [" + tilestring + @"]

                }";


            var content = JsonConvert.DeserializeObject(json);
            return Json(content);
        }


        
        [Route("[controller]/MapFile")]
        [HttpGet]
        public IActionResult Map()
        {
            var map = _gameMapRepo.GetLatest();
            var tiles = _gameTileRepo.GetIGameTilesByGMapID(map.GameMapID).ToList();
            
           // var content = JsonConvert.SerializeObject(_gameMapRepo.GetLatest());

            string tilestring = "";
            int counter = 1;
            foreach (GameTile gt in tiles)
            {
                tilestring += @"{'RoomID': " + gt.RoomID + @",'RoomName': '" + gt.RoomName + @"','RoomObject': '" + gt.RoomObject +
                    @"','xcoord': " + gt.xcoord + @",'ycoord': " + gt.ycoord + @",'N': " + gt.N +  @",'E': "+gt.E+ @",'S': " + gt.S + @",'W': " + gt.W + @" }";


                if (counter != tiles.LongCount())
                    tilestring += ",";
                counter++; 
            }
            string json =
             @"{'MapName': '" + map.MapName + @"','CreatedBy':'" + map.CreatedBy + @"','MaxHits':" + map.MaxHits + @",'MaxObjects':" + map.MaxObjects + @",'MapTiles': [" + tilestring+ @"]

                }";


              var content = JsonConvert.DeserializeObject(json);
            return Json(content);
        }




        [Route("[controller]/MapFile")]
        [HttpPost]
        public IActionResult Map([FromBody] GameMap inmap)
        {

            _gameMapRepo.CreateGameMap(inmap);
            //     inmap.MapTiles
         //   var mytiles = inmap.MapTiles.ToList();

            var map = _gameMapRepo.GetLatest();
            string json = @"{'Added':{'GameMapID':" + map.GameMapID + @",'MapName':'" +map.MapName +@"','CreatedBy':'" + map.CreatedBy + @"'}}";
            var content = JsonConvert.DeserializeObject(json);
            return Json(content);

        }


    }
}