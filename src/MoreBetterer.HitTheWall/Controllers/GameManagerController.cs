﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoreBetterer.HitTheWall.ViewModels;
using MoreBetterer.HitTheWall.Models.Repos;
using MoreBetterer.HitTheWall.Models;

namespace MoreBetterer.HitTheWall.Controllers
{
    public class GameManagerController : Controller
    {
    
        private readonly IGamePlayerRepo _gamePlayerRepo;

        private readonly IGameMapRepo _gameMapRepo;
        private readonly IGameTileRepo _gameTileRepo;

        public GameManagerController(IGameMapRepo gmapRepo, IGameTileRepo gtRepo, IGamePlayerRepo gamePlayerRepo)
        {
            _gameMapRepo = gmapRepo;
            _gameTileRepo = gtRepo;
            _gamePlayerRepo = gamePlayerRepo;

        }



        public IActionResult Index()
        {

            var myplayers = _gamePlayerRepo.GetGamePlayers().ToList();
            var tempViewModel = new GameMenuViewModel()
            {
                gamePlayers = myplayers

            };

            return View(tempViewModel);
        }


        public IActionResult CreatePlayer()
        {

           
            return View();
        }

        [HttpPost]
        public IActionResult CreatePlayer(GamePlayer player)
        {
            if (ModelState.IsValid)
            {
                player.PlayerStatus = "new";
                player.IsPlaying = false;
                _gamePlayerRepo.CreateGamePlayer(player);
            
            return RedirectToAction("Index");
        }


            return View();
        }

        public IActionResult GamePlayerDetailsView(int id)
        {
            var gamer = _gamePlayerRepo.GetByID(id);

            return View(gamer);
        }

        public IActionResult GameState(int id)
        {

            var gamer = _gamePlayerRepo.GetByID(id);
            string myname = "No Player Found";
            myname = gamer.PlayerName;
            var tempViewModel = new GameStateViewModel()
            {

                GamePlayer = gamer,
                playername = myname

            };

            return View(tempViewModel);
        }

        public IActionResult GameQuit(int id)
        {
            _gamePlayerRepo.GameQuit( id);

            return RedirectToAction("GameState", new { id = id });
        }

        

  public IActionResult PlayGame(int id)
        {
            var gamer = _gamePlayerRepo.GetByID(id);
            var gamemap = _gameMapRepo.GetByID(gamer.CurrentMap);
            var tiles = _gameTileRepo.GetIGameTilesByGMapID(gamemap.GameMapID).ToList();


            var currentTile = _gameTileRepo.GetByID(gamer.CurrentRoom);

            var norf = _gameTileRepo.GetByRoomAndMap(currentTile.N, gamemap.GameMapID);



            string victorycheck = _gamePlayerRepo.CheckVictory( id);


            if (victorycheck == "pending")
            {

                var tempViewModel = new GamePlayViewModel()
                {
                    player = gamer,

                    North = _gameTileRepo.GetByRoomAndMap(currentTile.N, gamemap.GameMapID),
                    East = _gameTileRepo.GetByRoomAndMap(currentTile.E, gamemap.GameMapID),
                    South = _gameTileRepo.GetByRoomAndMap(currentTile.S, gamemap.GameMapID),
                    West = _gameTileRepo.GetByRoomAndMap(currentTile.W, gamemap.GameMapID),
                    RoomName = currentTile.RoomName,
                    RoomID = currentTile.RoomID,
                    RoomObject = currentTile.RoomObject
                };
                return View(tempViewModel);
            }
            else if (victorycheck == "Lost")
            {
                _gamePlayerRepo.GameLost(id);
                var tempViewModel = new GamePlayViewModel()
                {
                    player = gamer,

                    North =0,
                    East = 0,
                    South =0,
                    West =0,
                    RoomName = "Game OVER",
                    RoomID = 0,
                    RoomObject = "You Have FAILED"
                };
                return View(tempViewModel);
            }
            else  
            {
                _gamePlayerRepo.GameWon(id);
                var tempViewModel = new GamePlayViewModel()
                {
                    player = gamer,

                    North = 0,
                    East = 0,
                    South = 0,
                    West = 0,
                    RoomName = "End Game",
                    RoomID = 0,
                    RoomObject = "WINNER!"
                };
                return View(tempViewModel);
            }



            
        }

        
        public IActionResult PlayGameMove(int id, int roomid,string path)
        {

            var player = _gamePlayerRepo.GetByID(id);

            if (roomid != 0)
            {

                GameTile mytile = _gameTileRepo.GetByID(roomid);

                string result = _gamePlayerRepo.MovePlayer(id, mytile, path);



            }
            else  {
                if (player.IsPlaying == false)
                {
                    return RedirectToAction("GameState", new { id = id });
                }


                    _gamePlayerRepo.oof(id);
            }
           
            

            return RedirectToAction("PlayGame", new { id = id });
        }










        public IActionResult StartNewGame (int id)
        {

            var gamer = _gamePlayerRepo.GetByID(id);
            var gamemap = _gameMapRepo.GetLatest();
            var tiles = _gameTileRepo.GetIGameTilesByGMapID(gamemap.GameMapID).ToList();

            _gamePlayerRepo.SelectMap(id, gamemap);

            Random rng = new Random();
        int n = tiles.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                GameTile value = tiles[k];
                tiles[k] = tiles[n];
                tiles[n] = value;
            }
          

            string tempstring = "";
            int counter = 1;
            foreach (GameTile gt in tiles)
            {
                if (counter <= gamemap.MaxObjects)
                    tempstring += gt.RoomObject + ";";

                counter++;
            }
            _gamePlayerRepo.SetObjectsNeeded(id,tempstring);
            _gamePlayerRepo.GameStart(id, tiles.Last());




            string myname = "No Player Found";
            myname = gamer.PlayerName;
            var tempViewModel = new GameStateViewModel()
            {

                GamePlayer = gamer,
                playername = myname

            };

            return RedirectToAction("PlayGame", new { id = id });
        }


    }
}