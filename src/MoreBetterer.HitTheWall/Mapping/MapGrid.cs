﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoreBetterer.HitTheWall.Mapping
{

    // see https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/arrays/multidimensional-arrays
    public class MapGrid
    {
        private int rows { get; set; }
        private int columns { get; set; }

        public MapGrid(int r_max, int c_max) {
            rows = r_max;
            columns = c_max;
        }

        
        public int[,] InitArray() {
            int[,] mygrid = new int[rows, columns];

            

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    mygrid[i, j] = 0;
                    
                }


            }
                                            
            
            return mygrid;
        }



        public int[,] UpdateGrid(List<MapTile> mapTiles)
        {
            int[,] mygrid = InitArray();


            foreach (var map in mapTiles)
            {
                mygrid[map.x, map.y] = map._RoomID;

            }


            return mygrid;
        }









    }
}
