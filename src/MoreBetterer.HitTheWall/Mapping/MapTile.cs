﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoreBetterer.HitTheWall.Mapping
{
    public class MapTile
    {
        public int x { get; set; }
        public int y { get; set; }
        public string _RoomName;
        public int _RoomID;


        public MapTile() { }
        public MapTile(int ix, int iy, int rid, string rname)
        {
            x = ix;
            y = iy;
            _RoomID = rid;
            _RoomName = rname;
        }
    }
}
