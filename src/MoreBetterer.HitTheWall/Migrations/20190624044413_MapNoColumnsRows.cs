﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MoreBetterer.HitTheWall.Migrations
{
    public partial class MapNoColumnsRows : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Columns",
                table: "GameMaps");

            migrationBuilder.DropColumn(
                name: "Rows",
                table: "GameMaps");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Columns",
                table: "GameMaps",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Rows",
                table: "GameMaps",
                nullable: false,
                defaultValue: 0);
        }
    }
}
