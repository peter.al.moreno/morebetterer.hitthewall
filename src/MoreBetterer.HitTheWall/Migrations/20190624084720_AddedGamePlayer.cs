﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MoreBetterer.HitTheWall.Migrations
{
    public partial class AddedGamePlayer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GamePlayers",
                columns: table => new
                {
                    GamePlayerID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PlayerName = table.Column<string>(maxLength: 20, nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 20, nullable: true),
                    CurrentMapMaxHits = table.Column<int>(nullable: false),
                    CurrentHits = table.Column<int>(nullable: false),
                    CurrentMapMaxObjects = table.Column<int>(nullable: false),
                    CurrentObjects = table.Column<int>(nullable: false),
                    CurrentRoom = table.Column<int>(nullable: false),
                    CurrentMap = table.Column<int>(nullable: false),
                    IsPlaying = table.Column<bool>(nullable: false),
                    PlayerStatus = table.Column<string>(nullable: true),
                    ObjectsCollected = table.Column<string>(nullable: true),
                    ObjectsNeeded = table.Column<string>(nullable: true),
                    PathTracker = table.Column<string>(nullable: true),
                    GamesWon = table.Column<int>(nullable: false),
                    GamesLost = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GamePlayers", x => x.GamePlayerID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GamePlayers");
        }
    }
}
