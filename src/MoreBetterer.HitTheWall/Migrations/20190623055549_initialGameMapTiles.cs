﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MoreBetterer.HitTheWall.Migrations
{
    public partial class initialGameMapTiles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GameTile",
                columns: table => new
                {
                    GameTileID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoomID = table.Column<string>(nullable: true),
                    RoomName = table.Column<string>(nullable: true),
                    xcoord = table.Column<int>(nullable: false),
                    ycoord = table.Column<int>(nullable: false),
                    GameMapID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameTile", x => x.GameTileID);
                    table.ForeignKey(
                        name: "FK_GameTile_GameMaps_GameMapID",
                        column: x => x.GameMapID,
                        principalTable: "GameMaps",
                        principalColumn: "GameMapID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GameTile_GameMapID",
                table: "GameTile",
                column: "GameMapID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GameTile");
        }
    }
}
