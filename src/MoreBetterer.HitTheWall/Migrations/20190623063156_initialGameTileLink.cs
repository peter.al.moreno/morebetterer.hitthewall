﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MoreBetterer.HitTheWall.Migrations
{
    public partial class initialGameTileLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameTile_GameMaps_GameMapID",
                table: "GameTile");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GameTile",
                table: "GameTile");

            migrationBuilder.RenameTable(
                name: "GameTile",
                newName: "GameTiles");

            migrationBuilder.RenameIndex(
                name: "IX_GameTile_GameMapID",
                table: "GameTiles",
                newName: "IX_GameTiles_GameMapID");

            migrationBuilder.AlterColumn<string>(
                name: "RoomName",
                table: "GameTiles",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RoomID",
                table: "GameTiles",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "GameMapID",
                table: "GameTiles",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "E",
                table: "GameTiles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "N",
                table: "GameTiles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "RoomObject",
                table: "GameTiles",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "S",
                table: "GameTiles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "W",
                table: "GameTiles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameTiles",
                table: "GameTiles",
                column: "GameTileID");

            migrationBuilder.AddForeignKey(
                name: "FK_GameTiles_GameMaps_GameMapID",
                table: "GameTiles",
                column: "GameMapID",
                principalTable: "GameMaps",
                principalColumn: "GameMapID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameTiles_GameMaps_GameMapID",
                table: "GameTiles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GameTiles",
                table: "GameTiles");

            migrationBuilder.DropColumn(
                name: "E",
                table: "GameTiles");

            migrationBuilder.DropColumn(
                name: "N",
                table: "GameTiles");

            migrationBuilder.DropColumn(
                name: "RoomObject",
                table: "GameTiles");

            migrationBuilder.DropColumn(
                name: "S",
                table: "GameTiles");

            migrationBuilder.DropColumn(
                name: "W",
                table: "GameTiles");

            migrationBuilder.RenameTable(
                name: "GameTiles",
                newName: "GameTile");

            migrationBuilder.RenameIndex(
                name: "IX_GameTiles_GameMapID",
                table: "GameTile",
                newName: "IX_GameTile_GameMapID");

            migrationBuilder.AlterColumn<string>(
                name: "RoomName",
                table: "GameTile",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "RoomID",
                table: "GameTile",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "GameMapID",
                table: "GameTile",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameTile",
                table: "GameTile",
                column: "GameTileID");

            migrationBuilder.AddForeignKey(
                name: "FK_GameTile_GameMaps_GameMapID",
                table: "GameTile",
                column: "GameMapID",
                principalTable: "GameMaps",
                principalColumn: "GameMapID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
