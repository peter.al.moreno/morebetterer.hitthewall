﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoreBetterer.HitTheWall.Mapping;
using MoreBetterer.HitTheWall.Models;
namespace MoreBetterer.HitTheWall.ViewModels
{
    public class MapGridViewModel
    {
 
        public int[,] grid { get; set; }
        public List<GameTile> GameTiles { get; set; }
        public string MapName { get; set; }
        public string CreatedBy { get; set; }
        public int Maxhits { get; set; }
        public int MaxObjects { get; set; }

    }
}
