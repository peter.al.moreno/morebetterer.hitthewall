﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoreBetterer.HitTheWall.Models;

namespace MoreBetterer.HitTheWall.ViewModels
{
    public class GamePlayViewModel
    {
        public GamePlayer player { get; set; }
        //  GameTile gameTile { get; set; }
        
      public int North { get; set; }
        public int East { get; set; }
        public int South { get; set; }
        public int West { get; set; }
        public string RoomName { get; set; }
        public int RoomID { get; set; }
        public string RoomObject { get; set; }


        //for posts
        public int id { get; set; }
        public int roomid { get; set; }
        public string path { get; set; }
    }
}
